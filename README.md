# Analise Taxi NYC

Projeto para respostas do processo seletivo da Data Sprints realizando várias análises de dados em corridas realizadas por táxis na cidade de Nova York nos anos entre 2009 e 2012.

### Pré-requisitos
Para executar os códigos e realizar as análises, utilizei o **Jupyter Notebook** e algumas  bibliotecas de python como: **Pandas, Matplotlib, Seaborn, Numpy e Plotly**.

Para rodar o código pode-se rodar através do próprio Jupyter e executar todas as células através do Restart & Run All Cells.