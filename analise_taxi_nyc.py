#!/usr/bin/env python
# coding: utf-8

# # Análise Táxis NYC
# Para começar a responder as perguntas propostas no desafio, começarei importando os datasets enviados juntamente com o desafio. 
# 
# ### Inicialização
# Usarei a biblioteca *Pandas* para realizar a importação em quatro dataframes distintos já que temos quatro arquivos separados por ano. Utilizarei as bibliotecas *Numpy* para algumas manipulações do dataframe e *Matplotlib* e *Seaborn* para apresentações gráficas.

# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

plt.style.use('ggplot')

import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)


# In[2]:


df2009 = pd.read_json('data-sample_data-nyctaxi-trips-2009-json_corrigido.json', lines=True, orient='columns')
df2010 = pd.read_json('data-sample_data-nyctaxi-trips-2010-json_corrigido.json', lines=True, orient='columns')
df2011 = pd.read_json('data-sample_data-nyctaxi-trips-2011-json_corrigido.json', lines=True, orient='columns')
df2012 = pd.read_json('data-sample_data-nyctaxi-trips-2012-json_corrigido.json', lines=True, orient='columns')


# Primeiramente irei concatenar todos os 4 dataframes para um único dataframe que iremos utilizar para responder algumas perguntas. 

# In[3]:


df4Years = pd.concat([df2009, df2010, df2011, df2012], ignore_index=True)


# In[4]:


df4Years.head() 


# In[5]:


df4Years.tail()


# ### Achando a distância média percorrida com no máximo 2 passageiros
# Para a primeira questão, queremos saber a distância média percorrida por viagens com no máximo 2 passageiros.
# 
# Com isso em mente vamos primeiro fazer uma limpeza dos dataframes criados para outros dataframes retirando as informações que não serão necessárias para análise. 
# Irei retirar as colunas de ***dropoff_datetime, fare_amount, payment_type, pickup_datetime, rate_code, store_and_fwd_flag, surcharge, tip_amount, tolls_amount, total_amount e vendor_id***. As colunas de latitude e longitude também serão retirtadas já que temos a coluna de ***trip_distance*** já com os dados de distância que utilizaremos para analisar. Sendo assim, iremos selecionar apenas as colunas ***trip_distance e passenger_count*** para o novo dataframe, para não afetar o dataframe original, separado apenas para a questão 01.

# In[6]:


q1 = df4Years[['passenger_count', 'trip_distance']]
q1.head()


# Precisamos filtrar o dataframe para apenas as corridas com no máximo 2 passageiros:

# In[7]:


q1 = q1[q1['passenger_count'] <= 2]


# In[8]:


q1.describe()


# Como temos números 0 de passageiros e 0 distâcia percorrida em uma viagem, conforme visto no **min** mostrado na tabela acima, precisamos saber a quantidade para tratar esses valores.

# In[9]:


q1['passenger_count'].value_counts()


# In[10]:


len(q1[q1['trip_distance'] == 0])


# A proporção entre as linhas com valores 0 e o total de linhas do dataframe é bem pequena, aproximadamente 32000 *entries* para 3 milhões *entries* totais (~ 1%), e por isso irei desconsiderar e remover essas linhas do dataframe para a análise.

# In[11]:


q1 = q1[(q1[['trip_distance', 'passenger_count']] != 0).all(axis=1)]


# In[12]:


q1.describe()


# In[13]:


plt.figure(figsize=(17,10))
sns.boxplot(y='trip_distance',
            data=q1,
            palette="colorblind",
            width=0.5)
plt.title('Boxplot Distâcia com no Máximo 2 Passageiros')
plt.show()


# Conforme mostrado pelo gráfico e pela tabela mostrada no *describe* conseguimos ver que a média percorrida pelos táxis com no máximo 2 passageiros é de **2,69 metros** aproximadamente. 

# ### Descobrindo os 3 maiores *vendors*
# Para a segunda questão, seguiremos alguns passos semelhantes à primeira questão.
# 
# Faremos a separação dos dados somente necessários para análise deixando somente as colunas **vendor_id** e **total_amount**.

# In[14]:


q2 = df4Years[['vendor_id', 'total_amount']]
q2.head()


# Para descobrir quem foi o maior *vendor* de NYC faremos um agrupamento por cada *vendor* e depois somar todos os valores adquiridos por corrida. Criarei uma função para poder reaproveitar na criação dos gráficos das outras resposta quando convir.

# In[15]:


def plot_chart (x_title, y_title, chart_title):
    plt.xlabel(x_title)
    plt.ylabel(y_title)
    plt.title(chart_title)
    plt.show()


# In[16]:


q2.groupby("vendor_id").sum().sort_values(by="total_amount", ascending=False)


# In[17]:


vendor_lookup = pd.read_csv('data-vendor_lookup-csv.csv')
vendor_lookup


# In[18]:


plt.figure(figsize=(17,10))
sns.countplot(x=q2['vendor_id'])
plot_chart('Empresa', 'Total Recebido', 'Valor Total Recebido por Empresa de TX')


# Com os valores lidos e através do gráfico mostrado acima podemos afirmar que o maior *vendor* foi a empresa **Creative Mobile Technologies, LLC** com o valor total aproximado de **19.550.000,00**.

# ### Histograma mensal das corridas pagas em dinheiro
# Para a terceira questão, faremos um histograma da distribuição mensal de corridas pagas em dinheiro.
# 
# Para realizar o histograma, deixaremos apenas as colunas de **payment_type** e utilizarei a coluna **dropoff_datetime** mas poderíamos utilizar a coluna de **pickup_datetime** também, porém minha escolha foi pelo fato de considerar o fim da corrida como a data do pagamento para a análise.

# In[19]:


q3 = df4Years[['payment_type', 'dropoff_datetime']]
q3.head()


# In[20]:


q3.dtypes


# Para conseguirmos trabalhar com os dado de data irei converter a coluna ***dropoff_datetime*** de um objeto para o formato *datetime* utilizando uma função da biblioteca Pandas.

# In[21]:


q3['dropoff_datetime'] = pd.to_datetime(q3['dropoff_datetime'], format='%Y-%m-%d')
q3.dtypes


# In[22]:


q3['payment_type'].unique()


# In[23]:


payment_lookup = pd.read_csv('data-payment_lookup-csv.csv')
payment_lookup.head(18)


# Para facilitar a limpeza dos dados desnecessários, como os pagamentos que não foram feitos com dinheiro, irei padronizar os valores de Cash e CASH para um único valor que será *"Cash"* e depois remover todo o restante dos pagamentos.

# In[24]:


q3['payment_type'] = q3['payment_type'].replace('CASH', 'Cash')
q3 = q3[q3['payment_type'] == 'Cash']
q3['payment_type'].unique()


# Faremos agora o histograma mensal das corridas pagas em dinheiro.

# In[25]:


plt.figure(figsize=(17,10))
histogram = plt.hist(q3['dropoff_datetime'], bins=48, color='#0504aa', alpha=0.7, rwidth=0.85)
plt.grid(axis='y', alpha=0.75)
plot_chart('Meses', 'Pagamentos em Dinheiro', 'Histograma Mensal das Corridas Pagas em Dinheiro')


# ### Série temporal da quantidade de gorjeta nos últimos 3 meses de 2012
# Para resposta da quarta questão, iremos fazer um gráfico de série temporal contando a quantidade de gorjetas de cada dia nos últimos 3 meses de 2012.
# 
# Para fazer o gráfico de série temporal iremos deixar apenas as colunas de **dropoff_datetime** e **tip_amount**.

# In[26]:


q4 = df2012[['dropoff_datetime', 'tip_amount']]
q4.head()


# Primeiramente, iremos fazer conforme a questão 3 e converter o tipo da coluna **dropoff_datetime** que está como objeto para o tipo de **data e hora(datetime)**. 

# In[27]:


q4['dropoff_datetime'] = pd.to_datetime(q4['dropoff_datetime'], format='%Y-%m-%d')
q4['dropoff_datetime'].dt.month.sort_values().unique()


# Precisamos agora isolar somente os dados dos últimos 3 meses (Agosto, Setembro e Outubro) de 2012 para iniciar a análise. Para facilitar na leitura farei um dicionário transformando os números 8, 9 e 10 dos meses da coluna *dropoff_datetime* em Agosto, Setembro e Outubro repectivamente em uma nova coluna que chamarei de Month. Irei separar os dias em uma nova coluna também para ficar mais fácil de juntar os dados em uma nova tabela e assim levantar a série temporal.

# In[28]:


q4['Month'] = q4['dropoff_datetime'].dt.month
q4['Day'] = q4['dropoff_datetime'].dt.day
q4.head()


# In[29]:


q4 = q4[(q4['Month'] == 8) | (q4['Month'] == 9) | (q4['Month'] == 10)]
q4['Month'].unique()


# Precisamos agora organizar nosso dataframe para uma ordem crescente no tempo e assim criar nosso gráfico de série temporal.

# In[30]:


time_serie = q4.groupby(['Month', 'Day']).sum()
time_serie


# In[31]:


plt.figure(figsize=(17,10))
time_serie.plot()
plt.grid(axis='y', alpha=0.75)
plot_chart('Últimos 3 meses de dados coletados de 2012', 'Gorjetas Recebidas por Dia', 'Série temporal da quantidade de gorjeta nos últimos 3 meses de 2012')


# ## Análises Extras 

# ### Tempo médio das corridas nos dias de sábado e domingo
# Para iniciar as respostas da questões bônus, irei utilizar as mesmas bases utilizadas nas questões anteriores.
# 
# Como queremos achar o tempo médio das corridas nos dias de sábado e domingo, precisarei tratar o dataframe para facilitar a análise. Irei deixar somente as colunas ***pickup_datetime e dropoff_datetime*** no novo dataframe que chamarei de **bonus1**.

# In[32]:


bonus1 = df4Years[['pickup_datetime', 'dropoff_datetime']]
bonus1.head()


# Irei primeiramente transformar as duas colunas no formato *datetime*. 

# In[33]:


bonus1['dropoff_datetime'] = pd.to_datetime(bonus1['dropoff_datetime'], format='%Y-%m-%d')
bonus1['pickup_datetime'] = pd.to_datetime(bonus1['pickup_datetime'], format='%Y-%m-%d')


# Criarei uma nova coluna com os dias da semana e depois retirar todos as *entries* que não são referentes aos dias sábado e domingo. 

# In[34]:


bonus1['weekday'] = bonus1['dropoff_datetime'].dt.weekday_name
bonus1.head()


# In[35]:


bonus1 = bonus1[(bonus1['weekday'] == 'Saturday') | (bonus1['weekday'] == 'Sunday')]
bonus1['weekday'].unique()


# Precisamos agora achar o tempo de cada viagem. Para isso, criarei uma nova coluna com a diferença de horário entre o *dropoff_datetime* e *pickup_datetime* com o resultado passado em **minutos**.

# In[36]:


bonus1['time_trip'] = bonus1['dropoff_datetime'] - bonus1['pickup_datetime']
bonus1['time_trip'] = bonus1['time_trip']/np.timedelta64(1,'m')
bonus1.head()


# In[37]:


bonus1['time_trip'].mean()


# Temos então que o tempo médio das corridas feitas nos dias sábado e domingo foram de **8,7 minutos**.

# In[38]:


bonus1 = bonus1[['weekday', 'time_trip']]
bonus1.head()


# In[39]:


plt.figure(figsize=(17,10))
sns.boxplot(y='time_trip',
            x='weekday',
            data=bonus1,
            palette="colorblind",
            width=0.5)
plot_chart('Dias da Semana', 'Tempo de viagem em minutos', 'Tempo Médio em Viagens Realizadas nos dias Sábado e Domingo')


# ### Mapa com os *pickups e dropoffs* no ano de 2010
# Para criar uma vizualização em mapa com as posições geográficas de *pickups e dropoffs* no ano de 2010, começarei criando um dataframe, chamado de bonus2, somente com as colunas **pickup_latitude, pickup_longitude, dropoff_latitude e dropoff_longitude**.

# In[40]:


bonus2 = df2010[['pickup_latitude', 'pickup_longitude', 'dropoff_latitude', 'dropoff_longitude']]
bonus2.head()


# Para conseguirmos a visualização no mapa, precisarei importar a biblioteca *Plotly*.

# In[48]:


import plotly.offline as py
import plotly.graph_objs as go

py.init_notebook_mode(connected=True)
mapbox_access_token = 'pk.eyJ1IjoieGFsZWgiLCJhIjoiY2p6Y25qazU3MDBneTNtcGx5Yml0ZzVidSJ9.TjdmVMM8Oq2Te-snPRZ9UQ'


# Para começar vou utilizar o ScatterGeo para gerar os pontos. Começarei colocando os pontos de *pickup* e depois adicionarei os de *dropoff*.

# In[54]:


data = [go.Scattermapbox(
                      lon = bonus2['pickup_longitude'],
                      lat = bonus2['pickup_latitude'],
                      name = 'Entrada do Passageiro',
                      mode = 'markers',
                      marker = dict(color = 'gold',size = 2.55, opacity = 0.5)),
        go.Scattermapbox(
                      lon = bonus2['dropoff_longitude'],
                      lat = bonus2['dropoff_latitude'],
                      name = 'Saída do Passageiro',
                      mode = 'markers',
                      marker = dict(color = 'cyan',size = 2.55, opacity = 0.5))]

layout = go.Layout(
                    title = '<b>Mapa de pickups em 2010</b>',
                    titlefont = {'family':'Arial','size': 24},
                    autosize = True,
                    hovermode = 'closest',
                    showlegend = False,
                    mapbox = dict(accesstoken = mapbox_access_token,
                                 bearing = 0,
                                 center = dict(lat = 40.75, lon = -74),
                                 pitch = 0,
                                 zoom = 12,
                                 style = 'light'))

fig = dict(data=data, layout=layout)


# In[55]:


py.iplot(fig, filename='Pickups&Dropoffs_NYC_TX')


# In[ ]:




